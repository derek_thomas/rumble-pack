﻿Rumble Pack 1.7026
06/09/2015

http://rumblepk.tumblr.com/

Credits

----------------------
PRODUCER / DIRECTOR
----------------------
DEREK THOMAS

----------------------
GAME DESIGNERS
----------------------
DEREK THOMAS
GERALD KAYE
JEREMIAH WARD

----------------------
GAME / SYSTEM PROGRAMMERS
----------------------
DEREK THOMAS

----------------------
CHARACTER DESIGN / ANIMATION / ILLUSTRATION
----------------------
DEREK THOMAS

----------------------
STAGE DESIGN
----------------------
DEREK THOMAS
WILL RUZICKA

----------------------
MUSIC AND SOUND EFFECTS
----------------------
DEREK THOMAS
GERALD KAYE

----------------------
VOICE ACTORS
----------------------
KRISTEN WOODS
CHARLES HAMWEY
DOMINNICK FORREST
GERALD KAYE

----------------------
QUALITY ASSURANCE
----------------------
JEREMIAH WARD
MIKE BYUN
CONNOR TRACY
GERALD KAYE
KRISTEN WOODS
DOMINNICK FORREST
MATTHEW JUNG
DAN CISNEROS
JEFFREY LAUFER
BRIAN O
RAZIEL SANCHEZ
DEXTER THOMAS
MATT SALKIN
DEWITT WARD
DANIEL NGUYEN

----------------------
SPECIAL THANKS
----------------------
MUGENGUILD.COM 
ELECBYTE
SHORYUKEN.COM

